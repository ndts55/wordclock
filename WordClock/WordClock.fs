namespace WordClock

module WordClock = 
    open System
    
    let private genLst n = List.replicate n true
    
    let private getHourLength n = 
        match n with
        | 11 -> 3
        | 0 | 6 | 12 -> 5
        | 7 -> 6
        | _ when n < 13 && n > 0 -> 4
        | _ -> 0
    
    let private getHourOffset n = 
        match n with
        | 1 -> 6
        | 2 -> 13
        | 3 -> 17
        | 4 -> 24
        | 5 -> 2
        | 6 -> 28
        | 7 -> 39
        | 8 -> 35
        | 9 -> 53
        | 10 -> 50
        | 11 -> 0
        | 0 | 12 -> 45
        | _ -> 0
    
    let hour (time : DateTime) = 
        let h = 
            if time.Minute >= 25 then (time.Hour + 1) % 12
            else time.Hour
        
        let l = getHourLength h
        let o = getHourOffset h
        List.replicate o false @ List.replicate l true @ List.replicate (58 - l - o) false
    
    let private getMinuteOffsetAndLength n = 
        match n with
        | _ when (n >= 0 && n < 5) || (n >= 30 && n < 35) -> (26, 0)
        | _ when (n >= 5 && n < 10) || (n >= 25 && n < 30) || (n >= 35 && n < 40) || (n >= 55 && n <= 59) -> (0, 4)
        | _ when (n >= 10 && n < 15) || (n >= 50 && n < 55) -> (4, 4)
        | _ when (n >= 15 && n < 20) || (n >= 45 && n < 50) -> (19, 7)
        | _ when (n >= 20 && n < 25) || (n >= 40 && n < 45) -> (8, 7)
        | _ -> (0, 26)
    
    let private vornach n = 
        match n with
        | 0 | 30 -> List.replicate 11 false
        | _ when n < 25 || (n >= 35 && n < 40) -> List.replicate 7 false @ List.replicate 4 true
        | _ when (n >= 25 && n < 30) || n >= 40 -> List.replicate 3 true @ List.replicate 8 false
        | _ -> List.replicate 11 false
    
    let private halb n = 
        if n >= 25 && n < 40 then List.replicate 4 true @ [ false ]
        else List.replicate 5 false
    
    let minutes (time : DateTime) = 
        let (o, l) = getMinuteOffsetAndLength time.Minute
        let number = List.replicate o false @ List.replicate l true @ List.replicate (26 - o - l) false
        number @ (vornach time.Minute) @ (halb time.Minute)
    
    let uhr (time : DateTime) = List.replicate 3 (time.Minute = 0)
    
    let dots (time : DateTime) = 
        let r = time.Minute % 5
        List.replicate r true @ List.replicate (4 - r) false
    
    let timeToBits (time : DateTime) = 
        let esist = [ true; true; false; true; true; true; false ]
        let minutes = minutes time
        let hour = hour time
        let uhr = uhr time
        let dots = dots time
        esist @ minutes @ hour @ uhr @ dots
    
    let letters = 
        [ 'e'; 's'; 'k'; 'i'; 's'; 't'; 'a'; 'f'; 'ü'; 'n'; 'f'; 'z'; 'e'; 'h'; 'n'; 'z'; 'w'; 'a'; 'n'; 'z'; 'i'; 'g'; 
          'd'; 'r'; 'e'; 'i'; 'v'; 'i'; 'e'; 'r'; 't'; 'e'; 'l'; 'v'; 'o'; 'r'; 'f'; 'u'; 'n'; 'k'; 'n'; 'a'; 'c'; 'h'; 
          'h'; 'a'; 'l'; 'b'; 'a'; 'e'; 'l'; 'f'; 'ü'; 'n'; 'f'; 'e'; 'i'; 'n'; 's'; 'x'; 'a'; 'm'; 'z'; 'w'; 'e'; 'i'; 
          'd'; 'r'; 'e'; 'i'; 'p'; 'm'; 'j'; 'v'; 'i'; 'e'; 'r'; 's'; 'e'; 'c'; 'h'; 's'; 'n'; 'l'; 'a'; 'c'; 'h'; 't'; 
          's'; 'i'; 'e'; 'b'; 'e'; 'n'; 'z'; 'w'; 'ö'; 'l'; 'f'; 'z'; 'e'; 'h'; 'n'; 'e'; 'u'; 'n'; 'k'; 'u'; 'h'; 'r'; 
          '.'; '.'; '.'; '.' ]
    
    let timeToLetters timeInMillis = 
        let bits = timeToBits timeInMillis
        if (List.length bits) = (List.length letters) then 
            List.zip bits letters
            |> List.filter (fun t -> fst t)
            |> List.map (fun t -> snd t)
        else []
