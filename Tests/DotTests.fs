module DotTests

open System
open WordClock
open Xunit

[<Theory>]
[<InlineData(0)>]
[<InlineData(5)>]
[<InlineData(10)>]
[<InlineData(15)>]
[<InlineData(20)>]
[<InlineData(25)>]
[<InlineData(30)>]
[<InlineData(35)>]
[<InlineData(40)>]
[<InlineData(45)>]
[<InlineData(50)>]
[<InlineData(55)>]
let ``0 dots``(minute) =
    let p = WordClock.dots (new DateTime(2000,3,4,10,minute,0)) |> List.toSeq
    let exp = List.replicate 4 false
    Assert.Equal(exp,p)

[<Theory>]
[<InlineData(1)>]
[<InlineData(6)>]
[<InlineData(11)>]
[<InlineData(16)>]
[<InlineData(21)>]
[<InlineData(26)>]
[<InlineData(31)>]
[<InlineData(36)>]
[<InlineData(41)>]
[<InlineData(46)>]
[<InlineData(51)>]
[<InlineData(56)>]
let ``1 dot``(minute) =
    let p = WordClock.dots (new DateTime(2000,3,4,10,minute,0)) |> List.toSeq
    let exp = [true] @ List.replicate 3 false
    Assert.Equal(exp,p)

[<Theory>]
[<InlineData(2)>]
[<InlineData(7)>]
[<InlineData(12)>]
[<InlineData(17)>]
[<InlineData(22)>]
[<InlineData(27)>]
[<InlineData(32)>]
[<InlineData(37)>]
[<InlineData(42)>]
[<InlineData(47)>]
[<InlineData(52)>]
[<InlineData(57)>]
let ``2 dots``(minute) =
    let p = WordClock.dots (new DateTime(2000,3,4,10,minute,0)) |> List.toSeq
    let exp = [true;true;false;false]
    Assert.Equal(exp,p)


[<Theory>]
[<InlineData(3)>]
[<InlineData(8)>]
[<InlineData(13)>]
[<InlineData(18)>]
[<InlineData(23)>]
[<InlineData(28)>]
[<InlineData(33)>]
[<InlineData(38)>]
[<InlineData(43)>]
[<InlineData(48)>]
[<InlineData(53)>]
[<InlineData(58)>]
let ``3 dots``(minute) =
    let p = WordClock.dots (new DateTime(2000,3,4,10,minute,0)) |> List.toSeq
    let exp = List.replicate 3 true @ [false]
    Assert.Equal(exp,p)
    
[<Theory>]
[<InlineData(4)>]
[<InlineData(9)>]
[<InlineData(14)>]
[<InlineData(19)>]
[<InlineData(24)>]
[<InlineData(29)>]
[<InlineData(34)>]
[<InlineData(39)>]
[<InlineData(44)>]
[<InlineData(49)>]
[<InlineData(54)>]
[<InlineData(59)>]
let ``4 dots``(minute) =
    let p = WordClock.dots (new DateTime(2000,3,4,10,minute,0)) |> List.toSeq
    let exp = List.replicate 4 true
    Assert.Equal(exp,p)