module TimeTests

open System
open WordClock
open Xunit

[<Fact>]
let ``zehn vor 1``() = 
    let p = WordClock.timeToLetters (new DateTime(2000, 3, 4, 12, 50, 0)) |> List.toSeq
    let exp = "esistzehnvoreins"
    Assert.Equal(exp, p)

[<Fact>]
let ``zehn nach mitternacht``() = 
    let p = WordClock.timeToLetters (new DateTime(2000, 3, 4, 0, 10, 0)) |> List.toSeq
    let exp = "esistzehnnachzwölf"
    Assert.Equal(exp, p)
