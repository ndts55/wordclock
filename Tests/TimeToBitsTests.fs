module TimeToBitsTests

open System
open WordClock
open Xunit

let private now = DateTime.Now

[<Fact>]
let ``returns list of booleans``() = 
    let p = WordClock.timeToBits now
    Assert.IsType<bool list>(p)

[<Fact>]
let ``returns list with length 114``() = 
    let p = WordClock.timeToBits now
    Assert.Equal(114, List.length p)

[<Fact>]
let ``always returns esist``() = 
    let p = WordClock.timeToBits now
    Assert.Equal([ true; true; false; true; true; true ], (List.toSeq (List.take 6 p)))

[<Fact>]
let ``certain fields always false``() = 
    let p = WordClock.timeToBits now
    let indizes = [ 2; 6; 48; 59; 60; 61; 70; 71; 72; 82; 83; 106 ]
    Assert.All([ for i in indizes do
                     yield p.[i] ], (fun i -> Assert.False(i)))
