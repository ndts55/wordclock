module MinutesTests

open System
open WordClock
open Xunit

[<Fact>]
let ``5 nach``() =
    let p = WordClock.minutes (new DateTime(2000,3,4,10,5,0)) |> List.toSeq
    let exp = List.replicate 4 true @ List.replicate 29 false @ List.replicate 4 true @ List.replicate 5 false
    Assert.Equal(exp, p)

[<Fact>]
let ``10 nach``() =
    let p = WordClock.minutes (new DateTime(2000,3,4,10,10,0)) |> List.toSeq
    let exp = List.replicate 4 false @ List.replicate 4 true @ List.replicate 25 false @ List.replicate 4 true @ List.replicate 5 false
    Assert.Equal(exp, p)

[<Fact>]
let ``15 nach``() =
    let p = WordClock.minutes (new DateTime(2000,3,4,10,15,0)) |> List.toSeq
    let exp = List.replicate 19 false @ List.replicate 7 true @ List.replicate 7 false @ List.replicate 4 true @ List.replicate 5 false
    Assert.Equal(exp, p)

[<Fact>]
let ``20 nach``() =
    let p = WordClock.minutes (new DateTime(2000,3,4,10,20,0)) |> List.toSeq
    let exp = List.replicate 8 false @ List.replicate 7 true @ List.replicate 18 false @ List.replicate 4 true @ List.replicate 5 false
    Assert.Equal(exp, p)

[<Fact>]
let ``5 vor halb``() =
    let p = WordClock.minutes (new DateTime(2000,3,4,10,25,0)) |> List.toSeq
    let exp = List.replicate 4 true @ List.replicate 22 false @ List.replicate 3 true @ List.replicate 8 false @ List.replicate 4 true @ [false]
    Assert.Equal(exp, p)

[<Fact>]
let ``halb``() =
    let p = WordClock.minutes (new DateTime(2000,3,4,10,30,0)) |> List.toSeq
    let exp = List.replicate 37 false @ List.replicate 4 true @ [false]
    Assert.Equal(exp, p)

[<Fact>]
let ``5 nach halb``() =
    let p = WordClock.minutes (new DateTime(2000,3,4,10,35,0)) |> List.toSeq
    let exp = List.replicate 4 true @ List.replicate 29 false @ List.replicate 8 true @ [false]
    Assert.Equal(exp, p)

[<Fact>]
let ``20 vor``() =
    let p = WordClock.minutes (new DateTime(2000,3,4,10,40,0)) |> List.toSeq
    let exp = List.replicate 8 false @ List.replicate 7 true @ List.replicate 11 false @ List.replicate 3 true @ List.replicate 13 false
    Assert.Equal(exp, p)

[<Fact>]
let ``15 vor``() =
    let p = WordClock.minutes (new DateTime(2000,3,4,10,45,0)) |> List.toSeq
    let exp = List.replicate 19 false @ List.replicate 10 true @ List.replicate 13 false
    Assert.Equal(exp,p)

[<Fact>]
let ``10 vor``() =
    let p = WordClock.minutes (new DateTime(2000,3,4,10,50,0)) |> List.toSeq
    let exp = List.replicate 4 false @ List.replicate 4 true @ List.replicate 18 false @ List.replicate 3 true @ List.replicate 13 false
    Assert.Equal(exp, p)

[<Fact>]
let ``5 vor``() =
    let p = WordClock.minutes (new DateTime(2000,3,4,10,55,0)) |> List.toSeq
    let exp = List.replicate 4 true @ List.replicate 22 false @ List.replicate 3 true @ List.replicate 13 false
    Assert.Equal(exp, p)

[<Fact>]
let ``punkt``() =
    let p = WordClock.minutes (new DateTime(2000,3,4,10,0,0)) |> List.toSeq
    let exp = List.replicate 42 false
    Assert.Equal(exp, p)
