module UhrTests

open System
open WordClock
open Xunit

[<Fact>]
let ``uhr on full clock``() = 
    let p = WordClock.uhr (new DateTime(2000, 3, 4, 10, 0, 0))
    Assert.All(p, (fun x -> Assert.True(x)))

[<Fact>]
let ``not uhr when not full clock``() = 
    let p = WordClock.uhr (new DateTime(2000, 3, 4, 10, 10, 0))
    Assert.All(p, (fun x -> Assert.False(x)))
