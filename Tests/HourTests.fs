module HourTests

open System
open WordClock
open Xunit

[<Fact>]
let ``eins``() = 
    let p = WordClock.hour (new DateTime(2000, 3, 4, 1, 0, 0)) |> List.toSeq
    let exp = List.replicate 6 false @ List.replicate 4 true @ List.replicate 48 false
    Assert.Equal(exp, p)

[<Fact>]
let ``zwei``() = 
    let p = WordClock.hour (new DateTime(2000, 3, 4, 2, 0, 0)) |> List.toSeq
    let exp = List.replicate 13 false @ List.replicate 4 true @ List.replicate 41 false
    Assert.Equal(exp, p)

[<Fact>]
let ``drei``() = 
    let p = WordClock.hour (new DateTime(2000, 3, 4, 3, 0, 0)) |> List.toSeq
    let exp = List.replicate 17 false @ List.replicate 4 true @ List.replicate 37 false
    Assert.Equal(exp, p)

[<Fact>]
let ``vier``() = 
    let p = WordClock.hour (new DateTime(2000, 3, 4, 4, 0, 0)) |> List.toSeq
    let exp = List.replicate 24 false @ List.replicate 4 true @ List.replicate 30 false
    Assert.Equal(exp, p)

[<Fact>]
let ``fünf``() = 
    let p = WordClock.hour (new DateTime(2000, 3, 4, 5, 0, 0)) |> List.toSeq
    let exp = List.replicate 2 false @ List.replicate 4 true @ List.replicate 52 false
    Assert.Equal(exp, p)

[<Fact>]
let ``sechs``() = 
    let p = WordClock.hour (new DateTime(2000, 3, 4, 6, 0, 0)) |> List.toSeq
    let exp = List.replicate 28 false @ List.replicate 5 true @ List.replicate 25 false
    Assert.Equal(exp, p)

[<Fact>]
let ``sieben``() = 
    let p = WordClock.hour (new DateTime(2000, 3, 4, 7, 0, 0)) |> List.toSeq
    let exp = List.replicate 39 false @ List.replicate 6 true @ List.replicate 13 false
    Assert.Equal(exp, p)

[<Fact>]
let ``acht``() = 
    let p = WordClock.hour (new DateTime(2000, 3, 4, 8, 0, 0)) |> List.toSeq
    let exp = List.replicate 35 false @ List.replicate 4 true @ List.replicate 19 false
    Assert.Equal(exp, p)

[<Fact>]
let ``neun``() = 
    let p = WordClock.hour (new DateTime(2000, 3, 4, 9, 0, 0)) |> List.toSeq
    let exp = List.replicate 53 false @ List.replicate 4 true @ [ false ]
    Assert.Equal(exp, p)

[<Fact>]
let ``zehn``() = 
    let p = WordClock.hour (new DateTime(2000, 3, 4, 10, 0, 0)) |> List.toSeq
    let exp = List.replicate 50 false @ List.replicate 4 true @ List.replicate 4 false
    Assert.Equal(exp, p)

[<Fact>]
let ``elf``() = 
    let p = WordClock.hour (new DateTime(2000, 3, 4, 11, 0, 0)) |> List.toSeq
    let exp = List.replicate 3 true @ List.replicate 55 false
    Assert.Equal(exp, p)

[<Fact>]
let ``zwölf``() = 
    let p = WordClock.hour (new DateTime(2000, 3, 4, 12, 0, 0)) |> List.toSeq
    let exp = List.replicate 45 false @ List.replicate 5 true @ List.replicate 8 false
    Assert.Equal(exp, p)
